#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

from postgres import Postgres
from praw import Reddit
from sqlite3 import connect, IntegrityError
from PIL import Image,ImageDraw,ImageFont
from time import time, strftime, sleep, gmtime
from datetime import datetime
from os.path import join, exists, dirname
from sys import argv

import secrets
import templates

DEBUG = "--debug" in argv
FORCE = "--force" in argv
DRY = "--dry" in argv

# Define local DB commands
creation_cmd = """PRAGMA foreign_keys=ON;
CREATE TABLE water_thread_id(
    post_id TEXT PRIMARY KEY NOT NULL,
    time_stamp REAL
);
CREATE TABLE voters(
	username TEXT PRIMARY KEY NOT NULL,
	yes_votes INTEGER,
	no_votes INTEGER,
	alignments INTEGER,
    waterings INTEGER,
    streak_record INTEGER,
    curr_yes_votes INTEGER,
    curr_no_votes INTEGER,
    curr_alignments INTEGER,
    curr_waterings INTEGER,
    curr_streak INTEGER,
    active INTEGER,
    last_voted TEXT,
    FOREIGN KEY(last_voted) REFERENCES water_thread_id(post_id)
);
CREATE TABLE thread_votes(
	thread_id TEXT NOT NULL,
	comment_id TEXT UNIQUE NOT NULL,
	comment_author TEXT,
	comment_text TEXT,
	comment_vote INTEGER,
	count_timestamp REAL,
    FOREIGN KEY(thread_id) REFERENCES water_thread_id(post_id), 
	FOREIGN KEY(comment_author) REFERENCES voters(username), 
    UNIQUE (thread_id,comment_author)
);"""

insert_thread_cmd = ("INSERT INTO `water_thread_id`(`post_id`,"
                     "`time_stamp`) VALUES (?,?);")

insert_vote_cmd = ("INSERT INTO `thread_votes` VALUES (?,?,?,?,?,?);")

tally_query = ("SELECT * FROM thread_votes "
              "WHERE thread_id = ? "
              "AND comment_vote = ? "
              "AND comment_author != 'takecareofmyplant';")

user_query = "SELECT * FROM voters WHERE username = ?"

get_records_query = ("SELECT username, {field} FROM voters"
           " WHERE {field} = (SELECT {operation}({field}) FROM voters)")

create_user_cmd = ("INSERT INTO `voters` "
                    "VALUES (?,0,0,0,0,0,0,0,0,0,0,0, NULL);")
# Can't parametrize set keys, seems like an oversight to me.
update_user_cmd = "UPDATE voters SET {} = ? WHERE username = ?"
update_streaks_cmd = ("UPDATE voters SET curr_streak = 0"
                     " WHERE last_voted != ?")

def update_user_n_fields_cmd(fields):
    """
    Creates a string to update more than one value at a time
    from a list of keys
    """
    inner = ""
    for f in fields:
        inner += " {0} =:{0},".format(f)
    return "UPDATE voters SET"+inner[:-1]+" WHERE username =:username"

def get_req(func, *args, **kwargs):
    """
    Get something. Try 5 times, then wait one minute for 10 minutes
    """
    overall = 0
    if DEBUG:
        print("get_req:",func.__name__,args,kwargs)
    while overall < 10:
        overall += 1
        attempt = 0
        while attempt < 5:
            try:
                if DEBUG:
                    out = func(*args, **kwargs)
                    print("get_req returning", func.__name__, out)
                    return out
                return func(*args, **kwargs)
            except Exception as E:
                attempt += 1
                if DEBUG:
                    print("get_req ERROR:",E,
                          "\nget_req:",overall,attempt)
            sleep(2)
        sleep(50)
    if DEBUG:
        print("get_req FAILED")
    return None

def get_votes(post_id):
    """
    Get the votes in a watering thread from the db
    Input a post id string
    Output # yeses, # nos, bool watered
    """
    ys = db.execute(tally_query,(post_id,1)).fetchall().__len__()
    ns = db.execute(tally_query,(post_id,-1)).fetchall().__len__()
    if ys != ns:
        result = {ys:1, ns:-1}.get(max(ys,ns))
    else:
        result = -1
    if DEBUG:
        print("get_votes:", post_id, ys, ns, ys+ns, result)
    return ys, ns, ys+ns, result

def update_db():
    """
    Get data from plant server, update local DB with information
    """
    # Get UTC times for the beginning and end of last month
    lm = gmtime(time()-14*24*60*60) # last month
    cm = gmtime() # current month
    if DEBUG:
        i = input("Fake month to next month?\n(Y/n) ==> ")
        if i != "n":
            cm = gmtime(time()+15*24*60*60)
    lmts = datetime(lm.tm_year,lm.tm_mon,1).timestamp() # timestamp
    cmts = datetime(cm.tm_year,cm.tm_mon,1).timestamp() # timestamp
    
    errors = False
    
    posts = get_req(P.all,"SELECT * FROM water_thread_id "+
             "WHERE time_stamp >= TO_TIMESTAMP({}) ".format(lmts)+
             "AND time_stamp <= TO_TIMESTAMP({})".format(cmts))
    if not posts:
        db.close()
        exit("Failed to get last months posts")
    votes = get_req(P.all,"SELECT * FROM thread_votes "+
             "WHERE count_timestamp >= TO_TIMESTAMP({}) ".format(lmts)+
             "AND count_timestamp <= TO_TIMESTAMP({})".format(cmts)+
             "AND comment_vote != 0")
    if not votes:
        db.close()
        exit("Failed to get last months votes")
    for post in posts:
        if not db.execute("SELECT * FROM water_thread_id "
                          "WHERE post_id = ?",(post.post_id,)
                    ).fetchone():
            # New post, add to DB
            db.execute(insert_thread_cmd,
                   (post.post_id,post.time_stamp.timestamp()))
        else:
            if DEBUG:
                print("Got an old watering thread...?",post)
    for vote in votes:
        if not db.execute("SELECT * FROM voters WHERE username = ?",
                     (vote.comment_author,)).fetchone():
            # New user, who dis?
            db.execute(create_user_cmd,(vote.comment_author,))
            if DEBUG:
                print("Added",vote.comment_author)
        if db.execute("SELECT * FROM water_thread_id WHERE post_id = ?",
                     (vote.thread_id,)).fetchone().__len__() == 0:
            if DEBUG:
                print("WARNING: Post collected from vote time period "
                  "but no record of that post! Ignored\n",post)
            errors = True
            continue
        try:
            db.execute(insert_vote_cmd,(
                vote.thread_id,
                vote.comment_id,
                vote.comment_author,
                vote.comment_text,
                vote.comment_vote,
                vote.count_timestamp
            ))
        except IntegrityError:
            if DEBUG:
                print("Vote already inserted")
    
    if errors and DEBUG:
        print("There were errors updating the database. Commit anyway?")
        i = input("(Y/n) ==> ")
        if i == "n":
            db.close()
            exit("aborted")
    db.commit()
    # return the timestamps for analysis step
    return lmts, cmts

def analyze_db(lmts, cmts):
    """
    Go through local database, calculate extra values for user
    """
    user_keys = ('username',
	'yes_votes',
	'no_votes',
	'alignments',
    'waterings',
    'streak_record',
    'curr_yes_votes',
    'curr_no_votes',
    'curr_alignments',
    'curr_waterings',
    'curr_streak',
    'active',
    'last_voted')
    thread_keys = ('thread_id','comment_id','comment_author',
                    'comment_text','comment_vote','count_timestamp')
    post_keys = ('post_id','time_stamp')
    
    for post in db.execute("SELECT * FROM water_thread_id "
                    "WHERE time_stamp >= ? "
                    "AND time_stamp <= ? "
                    "ORDER BY time_stamp ASC",(lmts,cmts)):
        post = {key:value for key,value in zip(post_keys, post)}
        if DEBUG:
            print("analyze_db: ", post['post_id'])
        ys, ns, total, result = get_votes(post['post_id'])
        # Save voting results to list for graph usage later
        m_day = gmtime(post['time_stamp']).tm_mday
        monthly_data[m_day] =  {
            'yes': ys,
            'no': ns,
            'total': total,
            'result': result
        }
        for vote in db.execute("SELECT * FROM thread_votes "
                    "WHERE thread_id = ?",(post['post_id'],)):
            vote = { key:value for key, value in zip(thread_keys, vote)}
            if DEBUG:
                print("   ",vote['comment_author'])
            user = db.execute(user_query,
                    (vote['comment_author'],)).fetchone()
            user = { key:value for key, value in zip(user_keys, user)}

            choice = {1:"yes_votes",-1:"no_votes"}.get(
                                vote['comment_vote'], None)
            if choice:
                user['last_voted'] = post['post_id']
                user['active'] = 1
                user['curr_streak'] += 1
                if user['curr_streak'] > user['streak_record']:
                    user['streak_record'] = user['curr_streak']
                user[choice] += 1
                user["curr_"+choice] += 1
                if vote['comment_vote'] == result:
                    user['alignments'] += 1
                    user['curr_alignments'] += 1
                    if result == 1:
                        user['waterings'] += 1
                        user['curr_waterings'] += 1
            db.execute(update_user_n_fields_cmd(user),user)
        db.execute(update_streaks_cmd,(post['post_id'],))
    db.commit()
    
    # Now comes the fun part
    records = {}
    
    longest_streak = db.execute(get_records_query.format(
                    operation="max",field="streak_record")).fetchall()
    records['streak'] = assign_record(longest_streak)
    
    most_yes = db.execute(get_records_query.format(
                    operation="max",field="yes_votes")).fetchall()
    most_no = db.execute(get_records_query.format(
                    operation="max",field="no_votes")).fetchall()
    most_votes = db.execute(get_records_query.format(
               operation="max",field="yes_votes + no_votes")).fetchall()
    records['yes'] = assign_record(most_yes)
    records['no'] = assign_record(most_no)
    records['votes'] = assign_record(most_votes)
    
    # Only do alignment checking on people with more than 10 votes
    math = "alignments * 100 / (yes_votes + no_votes)"
    alignment_query = ("SELECT username, {0} FROM voters WHERE {0} = "
                       "(SELECT max({0}) FROM voters WHERE "
                       "yes_votes + no_votes > 10) AND yes_votes "
                       "+ no_votes > 10".format(math))
    aligns = db.execute(alignment_query).fetchall()
    records['max_align'] = assign_record(aligns)
    antis = db.execute(alignment_query.replace("max","min")).fetchall()
    records['min_align'] = assign_record(antis)
    
    wet = db.execute(get_records_query.format(
                    operation="max",field="waterings")).fetchall()
    records['most_wet'] = assign_record(wet)

    # lastly, get the total votes
    records['total_votes'] = db.execute("SELECT "
                "sum(yes_votes + no_votes) FROM voters").fetchone()[0]
    if DEBUG:
        print("analyze_db success!", records)
    return records

def assign_record(query):
    return {
        'users': [ u for u,_ in query ],
        'value': query[0][1]
    }

def make_graph(data, records, lmts, cmts):
    """
    Make some cool graphage!
    """
    # calc some useful values
    t_votes = sum(data[day]['total'] for day in data)
    ty_votes = sum(data[day]['yes'] for day in data)
    tn_votes = sum(data[day]['no'] for day in data)
    m_votes = max(data[day]['total'] for day in data)
    n_days = round((cmts-lmts)/(24*60*60))
    unique_voters = len(db.execute("SELECT username FROM voters "
                                   "WHERE active = 1").fetchall())
    
    
    # Colors, based on the Pink Surfer palette
    # https://www.color-hex.com/color-palette/55339
    bkg = (216,238,226)
    contrast = (2,193,178)
    yes = (147,172,219)
    no = (240,113,170)
    other = (204,160,255)
    
    I = Image.new("RGBA",(1000,600),bkg)
    D = ImageDraw.Draw(I)
    # Thanks, Red Hat, for being cool!
    fp = join(BASEDIR, "overpass-mono-bold.otf")
    font = ImageFont.truetype(fp, 30)
    mid_font = font.font_variant(size=20)
    sm_font = font.font_variant(size=12)
    header = ("/r/TakeCareOfMyPlant "+strftime("%B %Y",gmtime(lmts))+
              " Voting Stats")
    sizing = D.textsize(header,font=font)
    D.text((500-int(sizing[0]/2),14),header,fill=other,font=font)
    
    # Make grid
    G = Image.new("RGBA",(900,372),bkg)
    GD = ImageDraw.Draw(G)
    Temp = Image.new("RGBA",(850,350),bkg)
    TMask = Image.new("L",(850,350),"white")
    TD = ImageDraw.Draw(TMask)
    for i in range(10):
        # Draw the text on the graph layer, graw the grid on the mask
        t = str(int(m_votes*(1-i/10)))
        GD.text((0,int(35*i)), " "*(3-len(t))+t, contrast, font=sm_font)
        TD.line((0,int(35*i),850,int(35*i)),0)
    GD.text((0,345),'  0',contrast,font=sm_font)
    # Easily manage different graph datas
    graphParts = {
        "yes": {
            "poly": [(850,350)],
            "M": Image.new("L",(850,350),"white"),
            "color": yes,
            "fill": 128
        },
        "no": {
            "poly": [(850,350)],
            "M": Image.new("L",(850,350),"white"),
            "color": no,
            "fill": 128
        },
        "total": {
            "poly": [(850,350)],
            "M": Image.new("L",(850,350),"white"),
            "color": other,
            "fill": None
        }
    }
    for day in range(n_days):
        Day = gmtime(lmts+day*24*60*60).tm_mday
        # X position
        X = int((day)/(n_days-1)*850)
        GD.text((X+20,360),str(Day),contrast,font=sm_font)
        TD.line((X,0,X,350),0)
        
        for part in graphParts:
            if not data.get(Day):
                graphParts[part]["poly"].append((X,350))
            else:
                graphParts[part]["poly"].append(
                    (X,350*(1-data[Day][part]/m_votes)))
    # Add grid to graph
    Temp = Image.composite(Temp,Image.new(
                            "RGBA",(850,350),contrast),TMask)
    for part in graphParts:
        # complete the polygon
        graphParts[part]["poly"].append((0,350))
        # Initiate drawing
        PD = ImageDraw.Draw(graphParts[part]["M"])
        PD.polygon(graphParts[part]["poly"],
                    fill=graphParts[part]["fill"])
        # Outline the polygon and apply the mask
        PD.line(graphParts[part]["poly"],width=3,
                    fill=graphParts[part]["fill"])
        Temp = Image.composite(Temp,Image.new("RGBA",(850,350),
                    graphParts[part]["color"]),graphParts[part]["M"])
    # Put the corrected graph transparency on the graph layer
    G.paste(Temp,(25,0),mask=Temp)
    # Make a frame around the graph
    for thickness in range(3):
        GD.rectangle(
            (25+thickness,0+thickness,875-thickness,350-thickness),
            None,contrast)
    # Paste it to full image
    I.paste(G,(80,70))
    # Make pie chart of yes vs no votes
    D.pieslice((10,460,110,560),-90,360*ty_votes/t_votes-90,fill=yes)
    D.pieslice((10,460,110,560),270-360*tn_votes/t_votes,270,fill=no)
    ys = str(ty_votes)
    ns = str(tn_votes)
    tl = str(t_votes)
    life = str(records['total_votes'])
    D.text((115,470),format("Total 'yes':","13")+
                    " "*(4-len(ys))+ys,fill=yes,font=mid_font)
    D.text((115,500),format("Total 'no':","13")+
                    " "*(4-len(ns))+ns,fill=no,font=mid_font)
    D.text((115,530),format("Total votes:","13")+
                    " "*(4-len(tl))+tl,fill=other,font=mid_font)
    D.text((115,560),"Lifetime Freyja votes: "+
                    life,fill=contrast,font=mid_font)
    uv = str(unique_voters)
    ls = str(records['streak']['value'])
    dr = str(sum(
           [data[d]['result'] for d in data if data[d]['result'] == 1]))
    D.text((355,470),"Unique voters:  "+
                    " "*(4-len(uv))+uv,fill=contrast,font=mid_font)
    D.text((355,500),"Longest streak: "+
                    " "*(4-len(ls))+ls,fill=contrast,font=mid_font)
    D.text((355,530),"# of drinks:    "+
                    " "*(4-len(dr))+dr,fill=contrast,font=mid_font)
    D.text((620,470),"Longest streak voters:",
                    fill=contrast,font=mid_font)
    
    from math import ceil
    awards = ""
    records['streak']['users'] = records['streak']['users']
    for user in records['streak']['users']:
        if D.textsize(awards+user+", ",font=sm_font)[0] > 350:
            awards += "\n"
        awards += user+", "
    D.text((620,500),awards[:-2],yes,font=sm_font)
    
    if DEBUG:
        I.show()
    fp = join(BASEDIR,strftime("%Y-%m.png",gmtime(lmts)))
    I.save(fp)
    return fp

def reddit(graph, records, lmts):
    subreddit = "takecareofmyplant"
    R = Reddit(
        client_id = secrets.clientID,
        client_secret = secrets.secret,
        password = secrets.password,
        user_agent = ("Making graphs for Freyja the plant for /r/"+
                        subreddit+" by /u/"+secrets.MY_REDDIT_USER),
        username = secrets.username
    )
    month = strftime("%B %Y",gmtime(lmts))
    # Get the number of total unique voters
    unique = db.execute("SELECT count(*) FROM voters").fetchone()[0]
    for part in ('votes','yes','no','max_align','min_align','most_wet'):
        records[part]['string'] = ""
        for user in records[part]['users']:
            records[part]['string'] += "* /u/{}\n".format(user)
    title = "Monthly Voting Data for "+month
    body = templates.post.format(
        month = month[:-5],
        t_votes = records['total_votes'],
        users = unique,
        most_votes = records['votes']['value'],
        most_votes_users = records['votes']['string'],
        most_yes = records['yes']['value'],
        most_yes_users = records['yes']['string'],
        most_no = records['no']['value'],
        most_no_users = records['no']['string'],
        most_wet = records['most_wet']['value'],
        most_wet_users = records['most_wet']['string'],
        most_align = records['max_align']['value'],
        most_align_users = records['max_align']['string'],
        most_anti = records['min_align']['value'],
        most_anti_users = records['min_align']['string'],
    )
    if DRY:
        print(title, graph)
        print(body)
        link = "DUMMY"
    else:
        p = R.subreddit(subreddit).submit_image(title, graph)
        p.reply(body)
        link = p.shortlink
    user_keys = ('username',
                'yes_votes',
                'no_votes',
                'alignments',
                'waterings',
                'streak_record',
                'curr_yes_votes',
                'curr_no_votes',
                'curr_alignments',
                'curr_waterings',
                'curr_streak',
                'active',
                'last_voted')
    for user in db.execute("SELECT * FROM voters "
                           "WHERE active = 1").fetchall():
        user = {key:val for key,val in zip(user_keys, user)}
        m_votes = user['curr_yes_votes']+user['curr_no_votes']
        PM = templates.pm.format(
            user = user['username'],
            link = link,
            yes_votes = user['curr_yes_votes'],
            yes_per = int(user['curr_yes_votes']/(m_votes)*100),
            no_votes = user['curr_no_votes'],
            no_per = int(user['curr_no_votes']/(m_votes)*100),
            m_align = user['curr_alignments'],
            wet = user['curr_waterings'],
            streak = user['curr_streak'],
            t_votes = user['yes_votes']+user['no_votes'],
            t_yes = user['yes_votes'],
            t_no = user['no_votes'],
            t_align = user['alignments'],
            t_wet = user['waterings'],
        )
        if DRY:
            if (user['username'].lower() == 
                secrets.MY_REDDIT_USER.lower()):
                print(PM)
        else:
            R.user(user['username']).message(
                "Your Monthly /r/TakeCareOfMyPlant Voting Stats",
                PM)

if __name__ == "__main__":
    BASEDIR = dirname(__file__)
    dbp = join(BASEDIR,"local.sqlite3")
    init = not exists(dbp)
    db = connect(dbp)
    if init:
        db.executescript(creation_cmd)
        db.commit()
    
    P = Postgres(secrets.POSTGRES_URL)
    
    # Reset everyone's activity
    db.execute("UPDATE voters SET active = 0, curr_streak = 0, "
               "streak_record = 0, curr_alignments = 0, "
               "curr_yes_votes = 0, curr_no_votes = 0, "
               "curr_waterings = 0")
    db.commit()
    
    monthly_data = {}
    lmts, cmts = update_db()
    records = analyze_db(lmts, cmts)
    # Do graphing
    graph = make_graph(monthly_data, records, lmts, cmts)
    # Upload it to Reddit, send out PMs
    reddit(graph, records, lmts)
