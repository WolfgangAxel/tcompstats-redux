# TakeCareOfMyPlant Monthly Statistics Bot

A bot to make graphs and send out voting statistics at the start of every month. Graphs look like this:

![A graph](example.png)