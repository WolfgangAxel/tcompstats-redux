post = """
# /r/TakeCareOfMyPlant Monthly Hall of Fame Update

As {month} draws to a close, Freyja has received {t_votes} votes from {users} different users in her lifetime.

These users hold the record for most votes cast ({most_votes}):

{most_votes_users}

These users hold the record for most `yes` votes cast ({most_yes}):

{most_yes_users}

These users hold the record for most `no` votes cast ({most_no}):

{most_no_users}

These users have watered Freyja the most ({most_wet} times):

{most_wet_users}

These users have the highest vote-to-outcome alignment of voters with 10 or more votes ({most_align}%):

{most_align_users}

These users have the lowest vote-to-outcome alignment of voters with 10 or more votes ({most_anti}%):

{most_anti_users}

See you all next month!!

^^RIP ^^Jeff ^^♥
"""

pm = """
Hello, /u/{user}!

Within the past month, you voted to care for Freyja the plant on /r/TakeCareOfMyPlant. [The subreddit-wide voting data can be viewed here]({link}). Here is some fun data I've compiled about your own voting records:

* You voted `yes` a total of {yes_votes} time(s), accounting for {yes_per}% of your votes
* You voted `no` a total of {no_votes} time(s), accounting for {no_per}% of your votes
* {m_align}% of your votes aligned with the watering result
* You were directly responsible for watering Jeff {wet} time(s)
* Your longest voting streak this month was {streak} day(s)

With this data, here are your updated lifetime voting statistics:

* You have voted {t_votes} times!
 * `Yes`: {t_yes}
 * `No`: {t_no}
* {t_align}% of your votes aligned with the watering results
* You have been directly responsible for watering Freyja {t_wet} time(s)!

Freyja looks forward to receiving your next vote!

"""
